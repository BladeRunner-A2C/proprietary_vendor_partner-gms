###############################################################################
# Please add "$(call inherit-product, vendor/partner_gms/products/personal_safety.mk)"
# to your product makefile to integrate the Personal Safety app.
#
PRODUCT_PACKAGES += \
    PersonalSafety \
    sysconfig_personal_safety \
    GmsConfigOverlayPersonalSafety

# Overlay for PersonalSafety
PRODUCT_PACKAGE_OVERLAYS += vendor/partner_gms/overlay/gms_overlay_personal_safety
